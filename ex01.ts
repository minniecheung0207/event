

let filepath = ('C:/TeckyCourse');
import fs from 'fs';
import path from 'path';


//1.promisify 的function的parameters 係同原本的CALLBACK method幾乎一樣，就是差了個callback
//2.承諾書不需要寫logic

function fsReadFilePromise(filepath: string) {//將條文檔path 放入去
    return new Promise<string[]>(function (resolve, reject) {//Promise<string[]>的<string[]>是看resolve的data type
        fs.readdir(filepath, function (err, files) {//前面裝錯誤信息，後面裝正確信息
                    //放你想read的東西 ，要對應paramenter
            if (err) {
                reject(err);
            }
            resolve(files); //只能resolve一個value。。。。今次想他return 反 filepath這個path 所有的信息
        })
    })
}

// call 他出來的2個公式
// //fsReadFilePromise(filepath).then((files) => {
// // console.log(files); 
// //})

// // async function test(){
//     let result=await fsReadFilePromise(filepath)
//     console.log(result)
// }

function fsstatPromise(filepath: string) {
    return new Promise<fs.Stats>(function (resolve, reject) {
        fs.stat(filepath, function (err, stat) {
            if (err) {
                reject(err);
            }
            resolve(stat);
        })
    })
}


//async
                //新的variable     //之前條path
export async function listAllJsRecursive(folderPath: string) {//如果這裏寫了peremeter，你就要在裏面使用。 你就要call 反巨的時候寫你想之後拿巨數值在裏面做什麽，
    try {       //新的variable
        let files = await fsReadFilePromise(folderPath);       //你想巨run哪個function就寫哪個，（）括號，你想讀什麽parameter，就是call function的value
        for (let file of files) {   
            const fullFolderPath = path.join(folderPath,file);//folderPath是前端，file是後端，加在一起裝在一個新的variable 度
            let filestat = await fsstatPromise(fullFolderPath);            
                if (path.extname(file) == ".js" && !filestat.isDirectory()) {
                    console.log(path.join(filepath, file))
                } else if (filestat.isDirectory()) {
                    await listAllJsRecursive(fullFolderPath)
                    
                }//距會拆左第一個folder先，然後第一次打開裏面的文檔之後fullFolderPath的path 網址已經變了，變了第1個文檔打開后第2個文檔的path入folderPath，再run
        }
       
}catch(e){
    console.log(e)
}
}

// listAllJsRecursive(filepath)